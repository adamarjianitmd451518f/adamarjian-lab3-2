/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian;

import edu.iit.sat.itmd4515.adamarjian.domain.Employee;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author adama
 */
public class Driver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //get out data access object 
        //this is what was apssed into out persistance.xml file
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("itmd4515PU");
        //get entity manager
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        // begin our transaction
       
//        //create a query
//        TypedQuery<Employee>  tq = em.createQuery("select e from Employee e", Employee.class);
//        tq.getResultList();
//        
//        List<Employee> results =  tq.getResultList();
//        //traverse the list
//        for(Employee e : results){
//            System.out.println(e.toString());     
//        }
//        
       
        tx.begin();
        //use persist instead of insert SQL command
        em.persist(new Employee("Alex", "Damarjian"));
        em.persist(new Employee("Another", "NewEmployee"));
        
        tx.commit();
        
         for(Employee e : em.createQuery("select e from Employee e", Employee.class).getResultList()){
            //end our transation
            System.out.println(e.toString());     
        } 
    }
    
}
